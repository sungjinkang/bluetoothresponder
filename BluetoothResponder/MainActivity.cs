﻿using System;
using System.Collections.Generic;
using System.IO;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using System.Linq;

namespace BluetoothResponder
{
    [Activity(Label = "Bluetooth Responder", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        ListView appsListView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);

            appsListView = FindViewById<ListView>(Resource.Id.AppsListView);
            appsListView.Adapter = new AppsListViewAdapter(this);
        }

        class AppsListViewAdapter : BaseAdapter<ResolveInfo>
        {
            string enabledActivitiesPath = Path.Combine(
                                               System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), 
                                               "enabledActivities");
            MainActivity activity;
            IList<ResolveInfo> resolveInfos;
            IList<string> enabledActivities;

            public AppsListViewAdapter(MainActivity activity)
            {
                if (File.Exists(enabledActivitiesPath))
                {
                    enabledActivities = File.ReadAllLines(enabledActivitiesPath).ToList();
                }
                else
                {
                    enabledActivities = new List<string>();
                }

                this.activity = activity;
                var intent = new Intent(Intent.ActionMain);
                intent.AddCategory(Intent.CategoryLauncher);
                resolveInfos = activity.PackageManager.QueryIntentActivities(intent, 0);
            }

            #region implemented abstract members of BaseAdapter

            public override long GetItemId(int position)
            {
                return position;
            }

            public override View GetView(int position, View convertView, ViewGroup parent)
            {
                var view = convertView ?? activity.LayoutInflater.Inflate(Resource.Layout.AppsListItem, null); // new ItemView(parent.Context);

                var appIcon = view.FindViewById<ImageView>(Resource.Id.AppIcon);
                var appLabel = view.FindViewById<TextView>(Resource.Id.AppLabel);
                var enableLaunchOnBluetooth = view.FindViewById<CheckBox>(Resource.Id.EnableLaunchOnBluetooth);

                var info = this[position];

                appIcon.SetImageDrawable(info.LoadIcon(activity.PackageManager));
                appLabel.Text = info.LoadLabel(activity.PackageManager);

                enableLaunchOnBluetooth.CheckedChange -= OnCheckChange;
                enableLaunchOnBluetooth.Tag = position;
                enableLaunchOnBluetooth.Checked = enabledActivities.FirstOrDefault(s => s == info.ActivityInfo.PackageName) != null;

                enableLaunchOnBluetooth.CheckedChange += OnCheckChange;

                return view;
            }

            void OnCheckChange(object sender, CompoundButton.CheckedChangeEventArgs e)
            {
                var view = sender as View;
                if (view != null)
                {
                    var index = (int)view.Tag;
                    var packageName = this[index].ActivityInfo.PackageName;
                    if (e.IsChecked)
                    {
                        enabledActivities.Add(packageName);
                    }
                    else
                    {
                        enabledActivities.Remove(packageName);
                    }
                }
                if (File.Exists(enabledActivitiesPath))
                {
                    File.Delete(enabledActivitiesPath);
                }
                File.WriteAllLines(enabledActivitiesPath, enabledActivities);
            }

            public override int Count
            {
                get
                {
                    return resolveInfos.Count;
                }
            }

            #endregion

            #region implemented abstract members of BaseAdapter

            public override ResolveInfo this [int index]
            {
                get
                {
                    return resolveInfos[index];
                }
            }

            #endregion

            class ItemView : LinearLayout
            {
                public ItemView(Context context)
                    : base(context)
                {
                    Inflate(context, Resource.Layout.AppsListItem, this);
                }
            }
        }
    }

}


