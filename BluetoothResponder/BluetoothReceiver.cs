using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Content.PM;
using Android.Bluetooth;
using System.IO;
using System.Linq;

namespace BluetoothResponder
{
    [BroadcastReceiver]
    [IntentFilter(new string[]{ BluetoothDevice.ActionAclConnected })]
    public class BluetoothReceiver : BroadcastReceiver
    {
        string enabledActivitiesPath = Path.Combine(
                                           System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments),
                                           "enabledActivities");

        #region implemented abstract members of BroadcastReceiver

        public override void OnReceive(Context context, Intent intent)
        {
            if (BluetoothDevice.ActionAclConnected == intent.Action)
            {
                
                if (File.Exists(enabledActivitiesPath))
                {
                    
                    var packageNames = File.ReadAllLines(enabledActivitiesPath).Where(s => !string.IsNullOrEmpty(s)).ToList();
                    var intents = new Intent[packageNames.Count];

                    for (int i = 0; i < packageNames.Count; i++)
                    {
                        intents[i] = context.PackageManager.GetLaunchIntentForPackage(packageNames[i]);
                        intents[i].AddFlags(ActivityFlags.NewTask);
                    }

                    context.StartActivities(intents);
                }


            }
        }

        #endregion

    }
}
